output "jumphost_sg_id" {
  value = aws_security_group.jumphost_sg.id
}

output "jenkins_sg_id" {
  value = aws_security_group.jenkins_sg.id
}

output "wordpress_sg_id" {
  value = aws_security_group.wordpress_sg.id
}

output "rds_sg_id" {
  value = aws_security_group.rds_rule.id
}