output "endpoint" {
    value = aws_db_instance.wordpressdb.endpoint
}

output "instance" {
  value = aws_db_instance.wordpressdb
}