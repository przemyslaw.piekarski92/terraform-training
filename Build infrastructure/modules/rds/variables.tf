variable "identifier" {
  default = "wordpressdb"
}
variable "storage" {
    default = 20
}
variable "engine" {
    default = "mysql"
}
variable "engine_version" {
    default = "8.0.32"
}
variable "subnet_group" {}
variable "sg"{}
variable "database_name" {}
variable "database_user"{}
variable "database_password" {}
variable "instance_class" {}