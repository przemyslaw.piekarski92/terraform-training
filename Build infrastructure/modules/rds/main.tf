resource "aws_db_instance" "wordpressdb" {
  identifier             = var.identifier
  allocated_storage      = var.storage
  engine                 = var.engine
  engine_version         = var.engine_version
  instance_class         = var.instance_class
  db_subnet_group_name   = var.subnet_group
  vpc_security_group_ids = [var.sg]
  db_name                = var.database_name
  username               = var.database_user
  password               = var.database_password
  skip_final_snapshot    = true

  # make sure rds manual password chnages is ignored
  lifecycle {
    ignore_changes = [password]
  }
}