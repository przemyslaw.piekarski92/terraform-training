output "jumphost_public_ip" {
  value = aws_instance.jumphost.public_ip
}
output "jenkins_public_ip" {
  value = aws_instance.jenkins_kibana.public_ip
}
output "jenkins_private_ip" {
  value = aws_instance.jenkins_kibana.private_ip
}
output "instance" {
  value = aws_instance.wordpress.id
}
output "wordpress_public_ip" {
  value = aws_instance.wordpress.public_ip
}
output "wordpress_private_ip" {
  value = aws_instance.wordpress.private_ip
}