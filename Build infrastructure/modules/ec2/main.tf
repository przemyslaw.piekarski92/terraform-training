data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu-pro-server/*"]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

resource "aws_instance" "jumphost" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.instance_type
  subnet_id                   = var.subnet_id_az1
  vpc_security_group_ids      = [var.sg_jh]
  associate_public_ip_address = true
  key_name                    = var.key_name

  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ubuntu"
    private_key = file(var.priv_key)
  }

  provisioner "file" {
    source      = var.priv_key
    destination = "/home/ubuntu/.ssh/bootcamp_key.pem"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod 600 /home/ubuntu/.ssh/bootcamp_key.pem",
      "exit"
    ]
  }
  
  tags = {
    "Name" = "Jumphost instance"
  }
}

resource "aws_instance" "jenkins_kibana" {
  ami = data.aws_ami.ubuntu.id
  instance_type = var.instance_type_jenkins
  subnet_id =  var.subnet_id_az2
  
  vpc_security_group_ids      = [var.sg_jenkins]
  associate_public_ip_address = true
  key_name                    = var.key_name
  root_block_device {
    volume_size = 25
  }
  user_data = file("entry-script-jenkins.sh")
  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ubuntu"
    private_key = file(var.priv_key)
  }

  provisioner "file" {
    source      = var.priv_key
    destination = "/home/ubuntu/.ssh/bootcamp_key.pem"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod 600 /home/ubuntu/.ssh/bootcamp_key.pem",
      "exit"
    ]
  }
  tags = {
    "Name" = "Jenkins/Kibana instance"
  }
}


resource "aws_instance" "wordpress" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = var.instance_type_wordpress
  subnet_id                   = var.subnet_id_az3
  vpc_security_group_ids      = [var.sg_wp]
  associate_public_ip_address = true
  key_name                    = var.key_name
  root_block_device {
    volume_size = 20
  }
  user_data                   = file("entry-script.sh")
  connection {
    type        = "ssh"
    host        = self.public_ip
    user        = "ubuntu"
    private_key = file(var.priv_key)
  }

  provisioner "file" {
    source      = var.priv_key
    destination = "/home/ubuntu/.ssh/bootcamp_key.pem"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod 600 /home/ubuntu/.ssh/bootcamp_key.pem",
      "exit"
    ]
  }
  depends_on = [var.db_instance]
  tags = {
    "Name" = "Wordpress instance"
  }

}