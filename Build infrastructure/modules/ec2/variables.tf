variable "instance_type" {}
variable "subnet_id_az1" {}
variable "subnet_id_az2" {}
variable "subnet_id_az3" {}
variable "sg_wp" {
    type = string
}
variable "key_name" {
    default = "bootcamp_key"
}
variable "script_name" {}
variable "db_instance" {}
variable "sg_jh" {
    type = string
}
variable "priv_key" {}
variable "instance_type_jenkins"{
    description = "Type of instance for Jenkins/Kibana server"
    default = "t2.xlarge"
}
variable "sg_jenkins" {
    type = string
}
variable "second_script_name" {}
variable "instance_type_wordpress"{
    description = "Type of Wordpress & Prestashop instance"
    default = "t2.medium"
}