variable "asz" {
  description = "AZs in the Region"
  type = list(string)
  default = [ "us-east-2a", "us-east-2b", "us-east-2c" ]
}
variable "vpc_cidr" {
    default = "10.0.0.0/16"
}
variable "main_vpc_name" {
    default = "Cloud VPC"
}


variable "cidr_public_subnet" {
    description = "CIDR for public subnets"
    type = list(string)
    default = [ "10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24" ]
}


variable "cidr_private_subnet" {
    description = "CIDR for private subnets"
    type = list(string)
    default = [ "10.0.4.0/24", "10.0.5.0/24" ]
}