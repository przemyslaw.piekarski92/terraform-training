terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}
#VPC
resource "aws_vpc" "cloud_vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  tags = {
    "Name" = "VPC: ${var.main_vpc_name}"
  }
}

#Public subnet for AZ1
resource "aws_subnet" "web_az1" {
  vpc_id                  = aws_vpc.cloud_vpc.id
  cidr_block              = var.cidr_public_subnet[0]
  map_public_ip_on_launch = true
  availability_zone       = var.asz[0]
  private_dns_hostname_type_on_launch = "resource-name"
  tags = {
    "Name" = "Web subnet AZ1"
  }
}

#Public subnet for AZ2
resource "aws_subnet" "web_az2" {
  vpc_id                  = aws_vpc.cloud_vpc.id
  cidr_block              = var.cidr_public_subnet[1]
  map_public_ip_on_launch = true
  availability_zone       = var.asz[1]
  private_dns_hostname_type_on_launch = "resource-name"
  tags = {
    "Name" = "Web subnet AZ2"
  }
}

#Public subnet for AZ3
resource "aws_subnet" "web_az3" {
  vpc_id                  = aws_vpc.cloud_vpc.id
  cidr_block              = var.cidr_public_subnet[2]
  map_public_ip_on_launch = true
  availability_zone       = var.asz[2]
  private_dns_hostname_type_on_launch = "resource-name"
  tags = {
    "Name" = "Web subnet AZ3"
  }
}


#First private subnet for RDS
resource "aws_subnet" "db_priv_first" {
  vpc_id                  = aws_vpc.cloud_vpc.id
  cidr_block              = var.cidr_private_subnet[0]
  map_public_ip_on_launch = false
  availability_zone       = var.asz[0]
  tags = {
    "Name" = "Db subnet priv first"
  }
}

#Second private subnet for RDS
resource "aws_subnet" "db_priv_second" {
  vpc_id                  = aws_vpc.cloud_vpc.id
  cidr_block              = var.cidr_private_subnet[1]
  map_public_ip_on_launch = false
  availability_zone       = var.asz[1]
  tags = {
    "Name" = "Db subnet priv second"
  }
}

#Create IGW
resource "aws_internet_gateway" "web-igw" {
  vpc_id = aws_vpc.cloud_vpc.id
}

#Create route table
resource "aws_route_table" "rt_vpc" {
  vpc_id = aws_vpc.cloud_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.web-igw.id
  }
}

#Asociate route table to public subnet Public1
resource "aws_route_table_association" "public_subnet1" {
  subnet_id      = aws_subnet.web_az1.id
  route_table_id = aws_route_table.rt_vpc.id
}

#Asociate route table to second public subnet Public2
resource "aws_route_table_association" "public_subnet2" {
  subnet_id = aws_subnet.web_az2.id
  route_table_id = aws_route_table.rt_vpc.id
}

#Asociate route table to second public subnet Public3
resource "aws_route_table_association" "public_subnet3" {
  subnet_id = aws_subnet.web_az3.id
  route_table_id = aws_route_table.rt_vpc.id
}


#Create RDS subnet group
resource "aws_db_subnet_group" "rds_subnet_group" {
  subnet_ids = ["${aws_subnet.db_priv_first.id}", "${aws_subnet.db_priv_second.id}"]
}