output "main_vpc_id" {
  value = aws_vpc.cloud_vpc.id
}

output "subnet_group" {
  value = aws_db_subnet_group.rds_subnet_group.id
}

output "subnet_id_az1" {
  value = aws_subnet.web_az1.id
}

output "subnet_id_az2" {
  value = aws_subnet.web_az2.id
}

output "subnet_id_az3" {
  value = aws_subnet.web_az3.id
}