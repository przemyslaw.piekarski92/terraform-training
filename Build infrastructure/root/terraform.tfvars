database_name           = "wordpress_db"
database_user           = "wordpress_user"
shared_credentials_file = "~/.aws/credentials"
region                  = "us-east-2"
vpc_cidr                = "10.0.0.0/16"
priv_key                = "~/.ssh/bootcamp_key.pem"
instance_type           = "t2.micro"
instance_class          = "db.t2.micro"
main_vpc_name           = "Cloud VPC"
script_name             = "./entry-script-jenkins.sh"
azs = [ "us-east-2a", "us-east-2b", "us-east-2c" ]
cidr_public_subnet = [ "10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24" ]
cidr_private_subnet = [ "10.0.4.0/24", "10.0.5.0/24" ]
instance_type_jenkins = "t2.xlarge"
database_password = "password"
second_script_name = "./entry-script.sh"
instance_type_wordpress = "t2.medium"