terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region                   = var.region
  shared_credentials_files = [var.shared_credentials_file]
  profile                  = "default"
}

module "vpc" {
  source = "../modules/vpc"
}

resource "aws_ecr_repository" "cloud_repo" {
  name = "cloud_task"
  image_tag_mutability = "IMMUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}

module "sg" {
  source = "../modules/sg"
  main_vpc_id = module.vpc.main_vpc_id
}
module "rds" {
  source = "../modules/rds"
  subnet_group = module.vpc.subnet_group
  sg = module.sg.rds_sg_id
  database_name = var.database_name
  database_user = var.database_user
  database_password = var.database_password
  instance_class = var.instance_class
}
module "ec2" {
  source = "../modules/ec2"
  instance_type = var.instance_type
  instance_type_jenkins = var.instance_type_jenkins
  instance_type_wordpress = var.instance_type_wordpress
  subnet_id_az1 = module.vpc.subnet_id_az1
  subnet_id_az2 = module.vpc.subnet_id_az2
  subnet_id_az3 = module.vpc.subnet_id_az3
  sg_wp = module.sg.wordpress_sg_id
  sg_jh = module.sg.jumphost_sg_id
  sg_jenkins = module.sg.jenkins_sg_id
  script_name = var.script_name
  second_script_name = var.second_script_name
  db_instance = module.rds.instance
  priv_key = var.priv_key
}