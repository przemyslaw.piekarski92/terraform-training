output "jumphost_public_ip" {
  value = module.ec2.jumphost_public_ip
}
output "jenkins_public_ip" {
  value = module.ec2.jenkins_public_ip
}
output "jenkins_private_ip" {
  value = module.ec2.jenkins_private_ip
}
output "wordpress_private_ip" {
  value = module.ec2.wordpress_private_ip
}
output "wordpress_public_ip" {
  value = module.ec2.wordpress_public_ip
}
output "db_endpoint" {
  value = module.rds.endpoint
}